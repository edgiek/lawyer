const links = document.querySelectorAll(
  ".navigation__inner > .navigation__list .navigation__item"
);
const hamburger = document.querySelector('.hamburger')
const hamburgerMenu = document.querySelector('.hamburger-menu')
const hamburgerList = document.querySelectorAll(".hamburger-menu > .navigation__list .navigation__item")

hamburgerList.forEach((el) => {
    el.addEventListener("click", () => {
        hamburgerMenu.classList.remove('active')
        hamburger.classList.remove('active')
    });
});

// links.forEach((el) => {
//   el.addEventListener("click", () => {
//     activeLink(el);
//   });
// });

function getActive() {
    if (window.location.hash !== "") {
        links.forEach((el) => {
            if (el.querySelector('.navigation__link').dataset.href === window.location.hash) el.classList.add("navigation__item--color")
            else el.classList.remove("navigation__item--color")
        });
    } else {
        links.forEach((el) => el.classList.remove("navigation__item--color"));
        links[0].classList.add('navigation__item--color')
    }
}

getActive()

// function activeLink(currentElement) {
//   links.forEach((el) => el.classList.remove("navigation__item--color"));
//   currentElement.classList.add("navigation__item--color");
// }

window.onhashchange = () => {
    getActive()
}



